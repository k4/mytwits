from django.conf.urls.defaults import patterns
from myTwits.views.mainpage.views import index
from myTwits.views.search.views import search, twitsSearch
from myTwits.views.application.views import application
from myTwits.views.testpage.views import testpage
import settings


# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    (r'^$', index),
    (r'^search/$', search),
    (r'^twitssearch/$', twitsSearch),
    (r'^application/$', application),
    (r'^test/$', testpage),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
   # (r'^app/firstapp$', ),
    # Example:
    # (r'^mytwits/', include('mytwits.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # (r'^admin/', include(admin.site.urls)),
)