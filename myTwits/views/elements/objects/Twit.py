

class Twit(object):
    def __init__(self, twitsCollection={}):
        self.dict = twitsCollection
    
    def setFields(self, resultTwit):
        self.dict = resultTwit
        
    def getField(self, fieldName):
        if self.dict.__contains__(fieldName):
            return self.dict[fieldName]
        else: 
            return ""
        
    def setField(self, fieldName, fieldValue):
        self.dict[fieldName] = fieldValue