import string
import random
import time
import urllib
import oauth.oauth as oauth


class SignatureGenerator(object):
    consumer_key = None
    consumer_secret = None

    def __init__(self, oauth_signature_method="HMAC-SHA1"):
        self.oauth_consumer = oauth.OAuthConsumer("9482fe0ZPTpErUNKWPvhmQ", "")
        self.oauth_token = oauth.OAuthToken("406373891-ebkFyqjWZlx6XvAufHbgjyrZI7j4npkLSt4XL4wI", "")
        self.parameters = {
            'oauth_nonce': nonceGenerator(),
            'oauth_timestamp': str(int(time.time())),
            'oauth_token': self.oauth_token.key,
            'oauth_consumer_key': self.oauth_consumer.key,
            'oauth_version': '1.0',
            'oauth_signature_method': oauth_signature_method
        }

    def getOAuthSignature(self, url, method):
        oauth_request = oauth.OAuthRequest(method, url, self.parameters)
        oauth_request.sign_request(oauth.OAuthSignatureMethod_HMAC_SHA1(), self.oauth_consumer, self.oauth_token)
        return oauth_request

    def getOAuthSignatureHeader(self, url, method="GET"):
        request = self.getOAuthSignature(url, method)
        authHeader = 'OAuth '
        firstItem = True
        if request.parameters:
            for k, v in request.parameters.iteritems():
                if k[:6] == 'oauth_':
                    authHeader = self.__addParameterToHeader(authHeader, k, v, firstItem)
                    if firstItem:
                        firstItem = False
        return {'Authorization': authHeader}

    def __addParameterToHeader(self, header, key, value, firstItem):
        sumStr = '%s="%s"'
        if not firstItem:
            header += ', '
        return header + sumStr % (key, escape(str(value)))

    def getOAuthSignaturePostData(self, url, method="GET"):
        return self.getOAuthSignature(url, method).to_postdata()

    def getOAuthSignatureUrl(self, url, method="GET"):
        return self.getOAuthSignature(url, method).to_url()

    def getParameters(self):
        return self.parameters


def escape(s):
    """Escape a URL including any /."""
    return urllib.quote(s, safe='~')


def nonceGenerator(size=32, chars=string.ascii_letters + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))