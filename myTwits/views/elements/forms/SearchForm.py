from django import forms

class SearchForm(forms.Form):
    query = forms.CharField()
    
    def setStyles(self):
        self._setStyle('query', 'search_input')
              
    def getForm(self):
        self.setStyles()
        return self
      
    def _setStyle(self, field, style):
        if not hasattr(self.fields[field].widget, 'attrs'):
            self.fields[field].widget.attrs = {}
        if not self.fields[field].widget.attrs.has_key('class'):
            self.fields[field].widget.attrs['class'] = style
            
    def setQuery(self, query):
        self['query'].value = query