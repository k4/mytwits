from django import template
from myTwits.views.elements.FieldsSet import FieldsSet
import re

register = template.Library()


@register.tag
def rendertwit(parser, token):
    nodelist = parser.parse('endrendertwit')
    parser.delete_first_token()
    return TwitRendererNode(nodelist)

class TwitRendererNode(template.Node):
    def __init__(self, nodelist):
        self.nodelist = nodelist
    
    def render(self, context):
        output = self.nodelist.render(context)
        output = self.__renderTwitLink(output)
        output = self.__renderTwitName(output)
        output = self.__renderTwitHashTag(output)
        return output
    
    def __renderTwitLink(self, input):
        p = re.compile("((http|https|ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s])", re.VERBOSE)
        return p.sub(r'<a href="\1" target="__blank">\1</a>', input)
    
    def __renderTwitName(self, input):
        p = re.compile('\@(\w+)', re.VERBOSE)
        return p.sub(r'<a href="{0}\1" target="__blank">@\1</a>'.format(FieldsSet().getStandardFieldSet()['twitterLink']), input)
    
    def __renderTwitHashTag(self, input):
        p=re.compile('[^&]\#(\w+)',re.VERBOSE)
        return p.sub(r'<a href="{0}?query=%23\1" target="__blank">#\1</a>'.format(FieldsSet().getStandardFieldSet()['search']), input)
    
   
    
    
    
    
    
    