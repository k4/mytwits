import httplib
from  django.utils.http import urlquote

class SimpleTwitterQueryHandler(object):
    
    def __init__(self, host):
        self.host = host
        self.params = {}
        self.address = ""
        self.response = None
        self.responseStatus = None
        self.requestException = None
    
    def setAddress(self, address):
        self.address = address
        
    def getAddress(self):
        return self.address
    
    def setHost(self, host):
        self.host = host
    
    def getHost(self):
        return self.host
    
    def setParams(self, params):
        self.params = params
        
    def getRequestString(self):
        return "http://" + self.host + self.getQueryString()
    
    def getQueryString(self):
        query = "&".join(map(self.__getQueryParam, self.params))
        return "/" + self.address + "?" + query
        
    def __getQueryParam(self, x):
        return x + "=" + urlquote(self.params[x])
    
    def doRequest(self):
        requestResult = self.__doRequest()
        if  requestResult and self.responseStatus == 200:
            pass
        elif requestResult:
            pass
        else:
            self.response = "({\"error\" : u'Some problem with connection'})"
            pass
        
    def __doRequest(self):
        self.requestException = None
        try:
            conn = httplib.HTTPConnection(self.host)
            conn.request("GET", self.getQueryString())
            response = conn.getresponse()
            self.responseStatus = response.status
            self.response = response.read()
            conn.close()
            return True
        except Exception as ex:
            self.requestException = ex
            return False
        
    def getResponse(self):
        if self.requestException is None:
            return self.response
        else:
            return self.__getErrorMessage()
        
    def __getErrorMessage(self):
        return "({u'error' : u'{0}'})".format(' '.join(self.request))            
            
            
            