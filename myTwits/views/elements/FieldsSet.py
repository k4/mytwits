import myTwits.catalogs.LinksCatalog as LinksCatalog
import myTwits.catalogs.FieldsCatalog as FieldsCatalog


class FieldsSet(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(FieldsSet, cls).__new__(cls)
        return cls.instance
    
    def getStandardFieldSet(self):
        return { 'siteName' : FieldsCatalog.SITENAME,
                 'mediaStyles' : LinksCatalog.MEDIA_STYLES_PATH,
                 'mediaScripts' : LinksCatalog.MEDIA_SCRIPTS_PATH,
                 'styleImages' : LinksCatalog.MEDIA_IMAGES_DARKSTYLE_PTH,
                 'images' : LinksCatalog.MEDIA_IMAGES_PATH,
                 'search' : LinksCatalog.SEARCH_PATH,
                 'index' : LinksCatalog.INDEX_PATH,
                 'siteUrl' : FieldsCatalog.SITEURL,
                 'twitterLink' : LinksCatalog.TWITTER_LINK }