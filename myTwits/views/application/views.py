from django.shortcuts import render_to_response
from myTwits.views.elements.FieldsSet import FieldsSet
from myTwits.views.elements.forms.SearchForm import SearchForm
from django.template.context import RequestContext
from myTwits.views.elements.SignatureGenerator import SignatureGenerator


def application(request):
    fieldsSet = FieldsSet()
    searchForm = SearchForm()
    content = getOAuthSignature()
    return render_to_response('application.tpl', dict(fieldsSet.getStandardFieldSet().items() +
                                                      {
                                                          'queryField': searchForm.getForm()['query'],
                                                          'oauthContent': content
                                                      }.items()),
                              context_instance=RequestContext(request))


def getOAuthSignature():
    sg = SignatureGenerator()
    request = sg.getOAuthSignatureHeader("https://api.twitter.com/oauth/request_token", "POST")
    return request