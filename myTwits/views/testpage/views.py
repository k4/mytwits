from django.shortcuts import render_to_response
from myTwits.views.elements.FieldsSet import FieldsSet
from myTwits.views.elements.forms.SearchForm import SearchForm
from django.template.context import RequestContext


def testpage(request):
    fieldsSet = FieldsSet()
    searchForm = SearchForm()
    return render_to_response('index.tpl', dict(fieldsSet.getStandardFieldSet().items() +
                                                {'queryField': searchForm.getForm()['query'],
                                                 'someContent': "Content"}.items()),
                              context_instance=RequestContext(request))


def getContent():
    pass