from myTwits.views.elements.forms.SearchForm import SearchForm
from myTwits.views.elements.handlers.SimpleTwitterQueryHandler import SimpleTwitterQueryHandler
from myTwits.views.elements.objects.Twit import Twit
import myTwits.catalogs.Parameters as Parameters
import json


class SearchHandler(object):
    def __init__(self, request):
        self.setRequest(request)

    def getSearchForm(self):
        try:
            return SearchForm(initial={'query': self.queryParams['q']}).getForm()
        except:
            return SearchForm().getForm()

    def getTwits(self):
        try:
            answer = self.__processQuery(self.queryParams)
            if answer is not None:
                jsonDecoder = json.JSONDecoder()
                pythonAnswer = jsonDecoder.raw_decode(answer)
                return self.__getTwitCollection(pythonAnswer)
        except:
            pass
        return list()

    def setRequest(self, request):
        self.queryParams = dict()
        self.request = request
        if request.GET.__contains__('query'):
            self.queryParams['q'] = request.GET['query']
        if request.GET.__contains__('twitnumber'):
            self.queryParams['max_id'] = int(request.GET['twitnumber']) - 1
        self.queryParams['rpp'] = Parameters.TWITS_COUNT

    def __processQuery(self, params):
        try:
            queryHandler = SimpleTwitterQueryHandler("search.twitter.com")
            queryHandler.setAddress("/search.json")
            queryHandler.setParams(params)
            queryHandler.doRequest()
            return queryHandler.getResponse()
        except:
            return None

    def __getTwitCollection(self, answer):
        collection = answer[0]
        if collection.__contains__('error'):
            return None
        else:
            twitsCollection = collection['results']
            return self.__generateTwits(twitsCollection)

    def __generateTwits(self, twitsCollection):
        twits = list()
        for twitSimple in twitsCollection:
            twit = Twit(twitSimple)
            twits.append(twit)
        return twits