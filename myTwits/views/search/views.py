from django.shortcuts import render_to_response
from myTwits.views.elements.FieldsSet import FieldsSet
from django.template.context import RequestContext
from myTwits.views.search.SearchHandler import SearchHandler
import myTwits.catalogs.Parameters as Parameters


def search(request):
    return resultPage(request, 'search.tpl')


def twitsSearch(request):
    return resultPage(request, 'twitlist.tpl')


def resultPage(request, template):
    fieldsSet = FieldsSet()
    searchHandler = SearchHandler(request)
    twits = searchHandler.getTwits()
    lastTwitId = ""
    twitsCount = 0
    if twits is not None and len(twits) > 0:
        lastTwitId = twits[-1].getField('id')
        twitsCount = len(twits)

    return render_to_response(template,
                              dict(fieldsSet.getStandardFieldSet().items() +
                                   {
                                       'twits': twits,
                                       'queryField': searchHandler.getSearchForm()['query'],
                                       'lastTwitId': lastTwitId,
                                       'twitsCount': twitsCount,
                                       'CONFIG_TWITS_COUNT': Parameters.TWITS_COUNT
                                   }.items()),
                              context_instance=RequestContext(request))