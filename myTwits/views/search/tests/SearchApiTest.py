import unittest
import httplib


class SearchApiTest(unittest.TestCase):

    def test_SimpleQueryNotEmptyReturn(self):
        conn = httplib.HTTPConnection("search.twitter.com")
        conn.request("GET", "/search.json?q=twitter")
        response = conn.getresponse()
        self.assertTrue(len(response.read()) != 0)


if __name__ == '__main__':
    unittest.main()