from myTwits.views.elements.FieldsSet import FieldsSet

class MyTwitsInitialization():
    
    def __init__(self, name=""):
        if name != "":
            self.name = name
            
    def init(self):
        """ initialize some module items """
        self.__initializeFieldsSet()
        self.__printResult()
        
    def __initializeFieldsSet(self):
        FieldsSet()
        
    def __printResult(self):
        if not hasattr(self, 'name'):
            print "Module started"
        else:                    
            print "Module '%s' started" % self.name
        
        
