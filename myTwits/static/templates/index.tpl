<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

    <head>
        <title>{{ siteName }}</title>
        <link rel="stylesheet" type="text/css" href="{{ mediaStyles }}dark_style.css">
        <script src="{{ mediaScripts }}twitterapi.js"></script>
        <script src="{{ mediaScripts }}jquery.min.js"></script>
        <script src="{{ mediaScripts }}func.js"></script>
    </head>

    <body>
        <div id="container">
            <div id="wrapper">
                {% include 'shadows.tpl' %}
                {% include 'header.tpl' %}
                <form id="search_form" action="{{ search }}">
                    {{ queryField }}
                    <input id="search_submit" type="image" src="{{ styleImages }}search.jpg"/> 
                </form>
                <div id="content">
                    {% block content %}
                    {{ someContent }}
                    {% endblock %}
                </div>
            </div>
        </div>
    </body>
</html>