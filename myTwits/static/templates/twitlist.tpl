{% load TwitRenderer %}

{% for twit in twits %}
	<div class="twit">
		{% include 'shadows.tpl' %}
		<div class="profile_block">
			<a href="{{ twitterLink }}{{ twit.dict.from_user }}" title="@{{ twit.dict.from_user }}" target="__blank">
				<img class="profile_img" src="{{ twit.dict.profile_image_url }}">
			</a>			
			<div class="from_to">
				<span class="from_user">
					<a href="{{ twitterLink }}{{ twit.dict.from_user }}" title="@{{ twit.dict.from_user }}" target="__blank">
						{{ twit.dict.from_user_name }}
					</a> @{{ twit.dict.from_user }}
				</span>
				{% if twit.dict.to_user %}
					<span class="to_user">
						<span class="gt">&gt;</span>
						<a href="{{ twitterLink }}{{ twit.dict.to_user }}" title="@{{ twit.dict.to_user }}" target="__blank">
							{{ twit.dict.to_user_name }} 
						</a>@{{ twit.dict.to_user }}
					</span>
				{% endif %}
			</div>
		</div>
		<div class="twit_text">
			{% rendertwit %}
				{{ twit.dict.text }}
			{% endrendertwit %}			
		</div>
		<div class="twit_date">
			{{ twit.dict.created_at }}	
		</div>
		<ul class="twit_buttons">
			<li><a class="button_link" href="{{ twitterLink }}{{ twit.dict.from_user }}/status/{{ twit.dict.id  }}" target="__blank">Tweet</a></li>
			<li><a class="button_link" href="{{ twitterLink }}intent/tweet?in_reply_to={{ twit.dict.id }}">Reply</a></li>
			<li><a class="button_link" href="{{ twitterLink }}intent/retweet?tweet_id={{ twit.dict.id }}">Retweet</a></li>
			<li><a class="button_link" href="{{ twitterLink }}intent/favorite?tweet_id={{ twit.dict.id }}">Favorite</a></li>
		</ul>
	</div>
{% endfor %}
{% if twitsCount != 0 %}
	<div id="load_twits">
		<div class="parameter" id="last_twit_number">{{ lastTwitId }}</div>
		<div class="parameter" id="amount">{{ twitsCount }}</div>
		<span id="load_twits_text">Show more twits</span>
		<span id="load_twits_gif"><img src="{{ images }}load.gif"></span>
	</div>
{% endif %}
