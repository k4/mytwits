<div id="header">
    <a href="{{ index }}" title="{{ siteName }}"><div id="title" title="{{ siteName }}"></div></a>
    <div id="follow_button">
        <a href="https://twitter.com/mytwits_rus" 
    	   class="twitter-follow-button" 
    	   data-show-count="false"
    	   data-size="large" 
    	   data-lang="en" 
    	   data-show-screen-name="false"
    	   data-align="right">Follow</a>	
    </div>
    <div id="about_mytwits_button">
    	<a href="https://twitter.com/share" 
    	class="twitter-share-button" 
    	data-lang="en"
    	data-url="{{ siteUrl }}"
    	data-count="none"
    	data-size="large"
    	data-via="myTwits_rus"
    	data-related="anywhere:{{ siteUrl }}"
    	data-text="Look what i found: different twitter apps">Tweet</a>
    </div>
</div>