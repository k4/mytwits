$(document).ready(function(){
	$('#load_twits').click(function(){
		addTwitsListener($(this));
	});
	
	$('#search_form').children('#id_query')
		.focus(function(){
			$('#search_form').attr('style', 'background: #707070;');
		})
		.blur(function(){
			$('#search_form').attr('style', 'background: none;');
		});
	
	function addTwitsListener(element){
		element.children('#load_twits_text').attr('style', 'display: none');
		element.children('#load_twits_gif').attr('style', 'display: block');
		var amount = element.children('#amount').text();
		var lastTwitNumber = element.children('#last_twit_number').text();
		var query = '/twitssearch/?' + 'query=' + $.getUrlVar('query') + '&twitnumber=' + lastTwitNumber + '&amount=' + amount;
		
		
		var request = $.ajax({
			url: query,
			type: 'GET'
		});
		
		request.done(function(msg){
			element.remove();
			$('#content').append(msg);
			$('#load_twits').click(function(){addTwitsListener($(this));});
		});
		
		request.fail(function(jqXHR, textStatus){
			alert("Request failed: " + textStatus);
			$('#load_twits').click(function(){addTwitsListener($(this));});
		});
	}
});

$.extend({
  getUrlVars: function(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name){
    return $.getUrlVars()[name];
  }
});