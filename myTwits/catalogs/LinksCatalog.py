from myTwits.settings import MEDIA_URL


INDEX_PATH = "/"
SEARCH_PATH = "/search/"
MEDIA_PATH = MEDIA_URL
MEDIA_IMAGES_PATH = MEDIA_URL + "images/"
MEDIA_SCRIPTS_PATH = MEDIA_URL + "scripts/"
MEDIA_STYLES_PATH = MEDIA_URL + "styles/"
TWITTER_LINK = "https://twitter.com/"
MEDIA_IMAGES_DARKSTYLE_PTH = MEDIA_IMAGES_PATH + "darkstyle/"    
