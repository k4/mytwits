
class ICatalog(object):

    def __new__(self):
        if not hasattr(self, 'instance'):
            self.instance = super(ICatalog, self).__new__(self)
            self.fields = dict()
        return self.instance
    
    def add(self, key, link):
        try:
            self.fields[key] = link
        except:
            pass
        
    def get(self, key):
        if key not in self.fields:
            return ""
        return self.fields[key] 
    
    def getAll(self):
        return self.fields
    
        